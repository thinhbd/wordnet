import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.StdIn;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * WordNet provide method to build Wordnet graph ,checking whether noun is in dictionary,
 * finding shortest common ancestor and distance between 2 nouns.
 */
public class WordNet {

    private HashMap<String , LinkedList<Integer>> wordList;
    private Digraph hypernymsGraph;
    private final String endLine = "\n";
    private final String whiteSpace = " ";
    private final String comma = ",";
    private int idMax;
    private ShortestCommonAncestor sca;
    private ArrayList<String> synsetsNouns; //araylist contain all nouns of the id

    public WordNet( WordNet wordNet )
    {
        this.wordList = wordNet.wordList;
        this.hypernymsGraph = wordNet.hypernymsGraph;
        this.idMax = wordNet.idMax;
        this.sca = wordNet.sca;
        this.synsetsNouns = wordNet.synsetsNouns;
    }
    /*public WordNet()
    {
        synsetsNouns = new ArrayList<String>();
    }*/


    // constructor takes the name of the two CSV input files
    public WordNet( String synsetsFileName, String hypernymsFileName ) throws IOException {

        synsetsNouns = new ArrayList<String>();
        wordList = new HashMap<String, LinkedList<Integer>>();
        BufferedReader in = null;
        try {
            in = new BufferedReader(new FileReader(synsetsFileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        idMax = 0;
        String line;

        while ( (line = in.readLine()) != null )
        {
            String[] tmp = line.split(comma);
            idMax = Integer.parseInt(tmp[0]);
            synsetsNouns.add(tmp[1]);
            String[] nouns = tmp[1].split(whiteSpace);
            LinkedList<Integer> idList = null;

            for( String str : nouns)
            {
                if( !wordList.containsKey(str) ) {
                    idList = new LinkedList<Integer>();
                }
                else idList = wordList.get(str);
                idList.add(idMax);
                wordList.put(str , idList);
            }
        }

        in.close();
        try {
            in = new BufferedReader(new FileReader(hypernymsFileName));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        hypernymsGraph = new Digraph(idMax + 2);
        while( (line = in.readLine()) != null )
        {
            String hypernymsSplit[] = line.split(comma);
            int id = Integer.parseInt(hypernymsSplit[0]);
            for (int i = 1; i < hypernymsSplit.length; i++) {
                int v = Integer.parseInt(hypernymsSplit[i]);
                hypernymsGraph.addEdge(id, v);
            }
        }

        sca = new ShortestCommonAncestor(hypernymsGraph);
        //System.out.println(sca.length(28075, 28075));
        //System.out.print(sca.getGraph().toString());
    }

    // all WordNet nouns
    /*public Iterable<String> nouns()
    {

    }*/

    // is the word a WordNet noun?
    public boolean isNoun(String word)
    {
        return wordList.containsKey(word);
    }

    // a synset (second field of synsets.txt) that is a shortest common ancestor
    // of noun1 and noun2
    public String sca(String noun1, String noun2)
    {
        if (!isNoun(noun1) || !isNoun(noun2))
            throw new IllegalArgumentException();
        int id = sca.ancestor(wordList.get(noun1) , wordList.get(noun2));
        return synsetsNouns.get(id);
    }

    // distance between noun1 and noun2
    public int distance(String noun1, String noun2)
    {
        if (!isNoun(noun1) || !isNoun(noun2))
        throw new IllegalArgumentException();

        return sca.length(wordList.get(noun1), wordList.get(noun2));
    }

    // do unit testing of this class
    public static void main(String[] args)
    {

    }
}