import edu.princeton.cs.algs4.Digraph;
import edu.princeton.cs.algs4.Queue;
import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdIn;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

/**
 * ShortestCommonAncestor provide method to find shortest ancestral path between two subset A and B.
 * An ancestral path between two vertices v and w in a direct graph is a directed path
 * from v to a common ancestor x, together with a directed path from w to the same ancestor x.
 * A shortest ancestral path is an ancestral path of minimum total length.
 */
public class ShortestCommonAncestor {

    //vertex color
    public final int RED = 0;
    public final int BLUE = 1;
    public final int GRAY = -1;

    private Digraph graph;
    public Digraph getGraph()
    {
        return graph;
    }

    // constructor takes a rooted DAG as argument
    public ShortestCommonAncestor( Digraph G )
    {
        graph = new Digraph(G);
    }

    public ShortestCommonAncestor( ShortestCommonAncestor sca )
    {
        this.graph = sca.graph;
    }
    public class Node
    {
        private int vertex , color;
        Node( int vertex , int color )
        {
            this.vertex = vertex;
            this.color = color;
        }
    }

    public class SCA
    {
        private int vertex , length;
        SCA( int vertex , int length )
        {
            this.vertex = vertex;
            this.length = length;
        }
    }

    // length and acestor of shortest ancestral path between subset A and B

    private SCA shortestAncestral( Iterable<Integer> A , Iterable<Integer> B )
    {
        int [][] vertexColor = new int[2][graph.V()];
        Queue queue = new Queue();

        /**
         * GRAY for none visited node
         * RED for node visited by node in subset A
         * BLUE for node visited by node in subset B
         */
        for( int i = 0 ; i < 2 ; i++ )
            Arrays.fill(vertexColor[i], GRAY);

        for( int v : A ) {
            queue.enqueue(new Node(v, RED));
            vertexColor[RED][v] = 0;
        }

        for( int w : B ) {
            queue.enqueue(new Node(w, BLUE));
            vertexColor[BLUE][w] = 0;
        }

        while ( !queue.isEmpty() )
        {
            Node front = (Node)queue.peek(); queue.dequeue();
            int u = front.vertex  , color = front.color;
            if( vertexColor[color][u] != GRAY && vertexColor[color ^ 1][u] != GRAY )
            {
                return new SCA(u , vertexColor[color][u] + vertexColor[color ^ 1][u]);
            }
            for( int adj : graph.adj(u) )
            {
                if( vertexColor[color][adj] == GRAY )
                {
                    vertexColor[color][adj] = vertexColor[color][u] + 1;
                    queue.enqueue(new Node(adj , color));
                }
            }

        }
        return new SCA( -1 , -1 );
    }


    // length of shortest ancestral path between v and w
    public static ArrayList intToIterbale( int u )
    {
        ArrayList array = new ArrayList(1);
        array.add(u);
        return array;
    }

    public int length( int v , int w )
    {
        SCA res;
        res = shortestAncestral( intToIterbale(v)  , intToIterbale(w));
        return res.length;
    }


    // a shortest common ancestor of vertices v and w
    public int ancestor(int v, int w)
    {
        SCA res;
        res = shortestAncestral( intToIterbale(v) , intToIterbale(w) );
        return res.vertex;
    }

    // length of shortest ancestral path of vertex subsets A and B
    public int length(Iterable<Integer> subsetA, Iterable<Integer> subsetB)
    {
        SCA res;
        res = shortestAncestral(subsetA , subsetB);
        return res.length;
    }

    // a shortest common ancestor of vertex subsets A and B
    public int ancestor(Iterable<Integer> subsetA, Iterable<Integer> subsetB)
    {
        SCA res;
        res = shortestAncestral(subsetA , subsetB);
        return res.vertex;
    }

    // do unit testing of this class
    public static void main(String[] args)
    {
        In in = new In(args[0]);
        Digraph G = new Digraph(in);
        ShortestCommonAncestor sca = new ShortestCommonAncestor(G);
        while (!StdIn.isEmpty()) {
            int v = StdIn.readInt();
            int w = StdIn.readInt();
            int length   = sca.length(v, w);
            int ancestor = sca.ancestor(v, w);
            StdOut.printf("length = %d, ancestor = %d\n", length, ancestor);
        }
    }
}
