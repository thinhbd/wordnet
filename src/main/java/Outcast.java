import edu.princeton.cs.algs4.In;
import edu.princeton.cs.algs4.StdOut;

import java.io.IOException;

/**
 * Outcast provide method outcast to determine the least related nouns of the input set.
 * Least related noun in the input set is the noun having maximum distance to other nouns.
 */
public class Outcast {

    private WordNet wordNet;
    public Outcast( WordNet wordNet )
    {
        this.wordNet = wordNet;
    }

    // given an array of WordNet nouns, return an outcast
    public String outcast(String[] nouns)
    {
        int maxDistance = 0;
        int index = -1;
        for( int i = 0 ; i < nouns.length ; i++ ) {
            int tmp = 0;
            for (int j = 0; j < nouns.length; j++)
                tmp += wordNet.distance(nouns[i] , nouns[j]);
            if( maxDistance < tmp ) {
                index = i;
                maxDistance = tmp;
            }
        }
        return nouns[index];
    }
    public static void main(String[] args) throws IOException {
        WordNet wordnet = new WordNet(args[0], args[1]);
        Outcast outcast = new Outcast(wordnet);
        for (int t = 2; t < args.length; t++) {
            In in = new In(args[t]);
            String[] nouns = in.readAllStrings();
            StdOut.println(args[t] + ": " + outcast.outcast(nouns));
        }
    }
}
