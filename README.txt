WordNet groups words into sets of synonyms called synsets and describes semantic relationships between them. 
One such relationship is the is-a relationship, which connects a hyponym 
(more specific synset) to a hypernym (more general synset). 
For example, animal is a hypernym of both bird and fish; bird is a hypernym of eagle, pigeon, and seagull.

Usage:
    Run Outcast with file location arguments to load hypernyms , 
    synsets file in folder Testcase 
    and other file contain nouns that we want find semantic relationships.